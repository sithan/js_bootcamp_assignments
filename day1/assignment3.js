//-----------------------------------------
//Assignment 3: Array of Objects Operations
//-----------------------------------------
let task = "Assignment 3: Array of Objects Operations";
console.log(task);

//1. Create an empty array called "cars".
let carsArray = [];

//2. Add three car objects to the "cars" array. Each car object should have the following properties:
//   - make: "Toyota"
//   - model: "Camry"
//   - year: 2018

carsArray[0] = {make:'Toyota',model:'Camry',year:2018};
console.log(carsArray[0]);


//3. Remove the first car object from the "cars" array.
delete carsArray[0];
console.log(carsArray);

//4. Add a new car object to the "cars" array with the following properties:
//   - make: "Honda"
//   - model: "Civic"
//   - year: 2020

carsArray[0] = {make:'Honda',model:'Civic',year:2020};
console.log(carsArray[0]);

//5. Update the "model" property of the second car object in the array to "Accord".
carsArray[0].model = 'Accord';
//console.log(carsArray);

//6. Print the final "cars" array after performing the above operations.
console.log(carsArray[0]);
